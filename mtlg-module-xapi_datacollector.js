"use strict";

/**
 * @namespace xapidatacollector
 * @memberOf MTLG
 *
 **/
window.CryptoJS = require("crypto-js");
require('xapiwrapper/src/xapi-util');
require('xapiwrapper/src/xapistatement');
require('xapiwrapper/src/xapiwrapper');

/**
 * @function init
 * @description Initialize the xapidatacollector module in MTLG.
 * @memberOf MTLG.xapidatacollector#
 *
 */
let init = function (options) {
    //Change configuration of XAPIWrapper to the one specified in the game configurations
    console.log('Initialize XAPI Module');
    if (options.xapi && options.xapi.endpoint && options.xapi.auth) {
        let lrsConfig = {
            'endpoint': options.xapi.endpoint,
            'auth': options.xapi.auth,
            'strictCallbacks' : true,
            'registration' : ADL.ruuid(),
        };
        if (options.xapi.gamename) lrsConfig.platform = options.xapi.gamename;
        if (options.xapi.actor) lrsConfig.actor = options.xapi.actor;
        ADL.XAPIWrapper.changeConfig(lrsConfig);
        console.log('XAPIWrapper configuration changed: ' + ADL.XAPIWrapper.testConfig());
    } else {
        console.log('No XAPI configuration options found');
    }

    //Register a Service Worker to deal with LRS unavailability
    console.log('Initialize Datacollector Service Worker');
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('./sw_xapi.js').then(function(registration) {
                // Registration was successful
                console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function(err) {
                // registration failed
                console.log('ServiceWorker registration failed: ', err);
            });
        });
    }
};


/**
 * @function info
 * @description info function is called from mtlg to get information about the modul
 * @memberOf MTLG.xapidatacollector#
 *
 * @return {object} returns standard object with name and note of the modul
 */
let info = function () {
    return{
        name: 'xapidatacollector',
        note: 'MTLG-Modul that adds a datacollector for XAPI data'
    }
};


/**
 * @function sendStatement
 * @description sendStatement function takes event data  and sends them as an XAPI statement to the LRS.
 * @memberOf MTLG.xapidatacollector#
 *
 * @param {object} stmt Needs at least a verb and an object attribute(if a default agent is not specified it needs an actor as well.
 *                          Can optionally also have a result and a context attribute.
 * @return {Promise} A Promise which resolves to the ID of the statement send, or throws an Error if rejected.
 */
let sendStatement = function(stmt){
    //Wrap function in a promise to make the function asynchronous
    return new Promise(function (resolve, reject) {
        //Generate a XAPI statement with an actor, verb and an object
        let xapiActor;
        try {
            if (!stmt.actor) xapiActor = createAgent("default");
            else xapiActor = createAgent(stmt.actor);
        }catch (e) {
            reject(e);
            return;
        }

        let xapiVerb;
        if (!stmt.verb) {
            reject(Error('No verb specified for xapi statement.'));
            return;
        }
        else xapiVerb = new ADL.XAPIStatement.Verb(stmt.verb.id, stmt.verb.display);

        let xapiObject;
        if (!stmt.object) reject(Error('Object missing from statement.'));
        else {
            if (!stmt.object.objectType) xapiObject = createActivity(stmt.object);
            else {
                try {
                    xapiObject = createAgent(stmt.object);
                }catch (e) {
                    reject(e);
                    return;
                }
            }
        }
        let xapiStmt = new ADL.XAPIStatement(xapiActor, xapiVerb, xapiObject);

        //Add result and context if present
        if (stmt.result) xapiStmt.result = stmt.result;
        if (stmt.context) xapiStmt.context = stmt.context;
        /*Using the xAPIWrapper to set the platform does not work, because no type check for activities is present,
        therefore it is done manually here*/
        if (ADL.XAPIWrapper.lrs.platform && (!stmt.object.objectType || stmt.object.objectType === "Activity")) {
            if (!stmt.context) xapiStmt.context = {};
            xapiStmt.context.platform = ADL.XAPIWrapper.lrs.platform;
        }

        xapiStmt.timestamp = new Date().toISOString();
        xapiStmt.generateId();

        //Send the generated XAPI statement and resolve the promise if successful.
        ADL.XAPIWrapper.sendStatement(xapiStmt, function(err, resp, obj){
            if (err){
                reject(Error(resp.responseText));
            } else {
                console.log(resp.status + ' - ' + resp.responseText);
                resolve(xapiStmt.id);
            }
        });
    });
};


/**
 * @function createAgent
 * @description createAgent function is a helper function to create an XAPI agent or group from an actor object.
 *
 * @param {object} actor An actor which can be an agent or group that will be converted into an XAPI agent object.
 *
 * @return {Agent} An XAPI agent or group with the properties of the actor object.
 */
let createAgent = function(actor){
    let xapiAgent;
    if (actor === "default"){
        let defaultActor = ADL.XAPIWrapper.lrs.actor;
        if (!defaultActor){
            throw new Error('No default actor specified in configuration');
        } else if (defaultActor.objectType === 'Agent'){
            xapiAgent = new ADL.XAPIStatement.Agent(defaultActor.id, defaultActor.name);
        } else if (defaultActor.objectType === 'Group'){
            xapiAgent = new ADL.XAPIStatement.Group(defaultActor.id, defaultActor.members, defaultActor.name);
        } else {
            throw  new Error('Default actor has no type');
        }
    } else if (actor.objectType === 'Agent') {
        xapiAgent = new ADL.XAPIStatement.Agent(actor.id, actor.name);
    } else if (actor.objectType === 'Group'){
        xapiAgent = new ADL.XAPIStatement.Group(actor.id, actor.members, actor.name);
    } else {
        throw  new Error('No valid actor passed');
    }
    return xapiAgent;
};


/**
 * @function createActivity
 * @description createActivity function is a helper function to create an XAPI activity from an activity object.
 *
 * @param {object} activity Activity object containing at least an activity id, but can also contain optional properties.
 *
 * @returns {Activity} An XAPI activity with an activity id and all optional properties of the activity object.
 */
let createActivity = function(activity){
    let xapiActivity = new ADL.XAPIStatement.Activity(activity.id);

    //Fill the XAPIActivity with all information which was passed
    if(activity.name || activity.description  || activity.type || activity.moreInfo || activity.extensions) xapiActivity = Object.assign({'definition':{}}, xapiActivity);

    if (activity.name) xapiActivity.definition.name = activity.name;

    if (activity.description) xapiActivity.definition.description = activity.description;

    if (activity.type) xapiActivity.definition.type = activity.type;

    if (activity.moreInfo) xapiActivity.definition.moreInfo = activity.moreInfo;

    if (activity.extensions) xapiActivity.definition.extensions = activity.extensions;

    return xapiActivity;
};


/**
 * @function changeDefaultActor
 * @description Function to change the default actor specified in the configurations of the LRS.
 *
 * @param {Object} actor The new default actor
 *
 * @return {boolean} True if changed successfully, false otherwise.
 */
let changeDefaultActor = function(actor){
     if (ADL.XAPIWrapper.testConfig()) {
         let oldConfig = ADL.XAPIWrapper.lrs;
         ADL.XAPIWrapper.changeConfig({actor: actor});
         //Check if nothing broke
         if (ADL.XAPIWrapper.testConfig()) {
             return true;
         } else {
             //If something broke try to recover previous state
             ADL.XAPIWrapper.changeConfig(oldConfig);
             return false;
         }
     } else {
         console.log('No correct LRS configuration present');
         return false;
     }
};


/**
 * @function getStatementById
 * @description Function to find a statement in a LRS by its statement ID.
 * @memberOf MTLG.xapidatacollector#
 *
 * @param {string} statementId The ID of the statement the LRS is queried for.
 *
 * @return {Promise} A Promise with the XAPI statement to which the statement ID belongs. If no statement with the ID is found in
 *                              the LRS then the Promise is rejected with an error.
 */
let getStatementById = function(statementId){
    return new Promise(function(resolve, reject) {
        if (statementId){
            //Let wrapper create the array for correct syntax
            let searchParams = ADL.XAPIWrapper.searchParams();
            searchParams['statementId'] = statementId;
            //Return the statement in the callback of the request
            ADL.XAPIWrapper.getStatements(searchParams, null, function (err, res, body) {
                if (err) {
                    reject(Error('Not able to retrieve statement with id:' + statementId));
                    return;
                }
                if (res && body) {
                    resolve(body);
                } else {
                    reject(Error('Not able to retrieve statement with id:' + statementId));
                }
            });
        } else reject(Error('No statement ID specified'));
    });
};


/**
 * @function getStatements
 * @description Function to find all statements in a LRS with the given search criteria.
 * @memberOf MTLG.xapidatacollector#
 *
 * @param {object} [searchParams] The search criteria for statements. Use the right keywords to name the parameters. Look
 *                                  at https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#partthree
 *                                  for parameters which can be searched for. If no criteria are specified, look up all
 *                                  statements (not advised because of potentially very large response).
 *
 * @return {Promise} A Promise with an array of XAPI statements fulfilling the search criteria. If no statements fulfill
 *                      the criteria or an error occurred while retrieving statements, the Promise is rejected with an Error.
 */
let getStatements = function(searchParams){
    return new Promise(function(resolve, reject) {
        let statementArray = [];
        if (searchParams) {
            //Let wrapper create the array for correct syntax
            let search = ADL.XAPIWrapper.searchParams();
            for (let param in searchParams) {
                search[param] = searchParams[param];
            }
            //Recursively add all statements to statementArray by invoking a new request in the callback if more statements can be retrieved
            ADL.XAPIWrapper.getStatements(search, null, function getAllStatements(err, res, body) {
                if (err) {
                    reject(Error('Error while retrieving statements with search parameters:' + JSON.stringify(searchParams)));
                    return;
                }
                if (body["0"] && body["0"].message) {
                    reject(Error(body["0"].message));
                    return;
                }
                for (let stmt in body.statements) {
                    statementArray.push(body.statements[stmt]);
                }
                if (body.more && body.more !== "") {
                    ADL.XAPIWrapper.getStatements(search, body.more, getAllStatements)
                }
                //Resolve or reject the promise depending on if statements were found
                if (statementArray.length > 0) resolve(statementArray);
                else reject(Error('No statements found with the search parameters:' + JSON.stringify(searchParams)));
            });
        } else {
            ADL.XAPIWrapper.getStatements(null, null, function getAllStatements(err, res, body) {
                if (err) {
                    reject(Error('Error while retrieving statements with search parameters:' + JSON.stringify(searchParams)));
                    return;
                }
                if (body["0"] && body["0"].message) {
                    reject(Error(body["0"].message));
                    return;
                }
                for (let stmt in body.statements) {
                    statementArray.push(body.statements[stmt]);
                }
                if (body.more && body.more !== "") {
                    ADL.XAPIWrapper.getStatements(null, body.more, getAllStatements)
                }
                //Resolve or reject the promise depending on if statements were found
                if (statementArray.length > 0) resolve(statementArray);
                else reject(Error('No statements found with the search parameters:' + JSON.stringify(searchParams)));
            });
        }

    });
};

MTLG.xapidatacollector = {
    init,
    sendStatement:          sendStatement,
    getStatementById:       getStatementById,
    getStatements:          getStatements,
    changeDefaultActor:     changeDefaultActor
};
MTLG.addModule(init, info);
