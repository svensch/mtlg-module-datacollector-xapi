
# xAPI Datacollector

Das xAPI Datacollector Modul kann benutzt werden, um Aktionen der Spieler im Spiel als Events festzuhalten und
an eine Datenbank zu schicken. Mit der Hilfe von Learning Analytics Verfahren können aus diesen Daten Indikatoren
zu der Leistung eines Spielers und entsprechendes Feedback erstellt werden. Das verwendete Format ist dabei der
Experience API (xAPI) Standard. Die komplette Spezifikation des Standards kann [in diesem GitHub Repository](https://github.com/adlnet/xAPI-Spec"xAPI Spezifikation")
eingesehen werden. Der Datacollector ermöglicht auch Statements, wie die Datensätze in xAPI genannt werden,
von der Datenbank zu erfragen.

## Moduleinbindung

Im ersten Schritt wird das Modul wie jedes andere MTLG Modul auch eingebunden. Es wird als dependency in der
`package.json` des Spiels hinzugefügt und dann mit dem `require()` Befehl in der `game.js` hinzugefügt. Zusätzlich
müssen aber noch die Spieloptionen erweitert werden. Dazu wird dem `MTLG.loadOptions()` Befehl in der `game.config.js`
ein zusätzliches xAPI Objekt übergeben. Ein Beispiel wie so ein xAPI Objekt aussehen kann ist in der
[game.config.js](demo/dev/manifest/game.config.js)
des Demo Spiels gezeigt. Die `endpoint` und `auth` Attribute sind notwendig, damit das Modul funktioniert. Die `gamename`
und `actor` Attribute hingegen sind optional.

Der Endpoint legt die Datenbank fest, an die der Datacollector die Statements schickt. Für den Learning Locker,
den der Lehrstuhl als Datenbank für seine xAPI Statements nutzt, ist der Endpoint <http://lrs.elearn.rwth-aachen.de/data/xAPI/>.
Die Website des Learning Lockers kann [hier](http://lrs.elearn.rwth-aachen.de/login) gefunden werden.
Ein Authorization Token bekommt man im Learning Locker unter `Settings` und dann `Clients`. Entweder man nutzt einen
bestehenden Client oder legt einen Neuen an.

Die optionalen Attribute können genutzt werden, um den Spielnamen zu bestimmen, welcher bei jedem Statement mit einem Activity Objekt als `platform`
Attribut mitgesendet wird, oder um einen Default Actor festzulegen, welcher immer benutzt wird, falls kein Akteur für ein
Statement festgelegt wurde. Dieser Default Actor kann auch nachträglich zur Runtime mit der Funktion `changeDefaultActor(newDefaultActor)`
gesetzt oder verändert werden.

## Modulnutzung

### Statements Senden

Um ein Statement an die Datenbank zu senden, wird der Aufruf `MTLG.datacollector.sendStatement(stmt)` verwendet. Das Statememt
Objekt, welches der Funktion als Parameter übergeben wird, beinhaltet die meisten relevanten Attribute eines xAPI Statement.
Es ist dabei notwendig, dass die Attributenamen und ihre Datentypen mit der [xAPI Spezifikation](https://github.com/adlnet/xAPI-Spec"xAPI Spezifikation")
konform sind. Um die IDs für spezielle Verben und Aktivitäten im Kontext von Multi-Touch Learning Games zu erhalten, kann
die [xAPI Statement Registry für MTLG Events](https://registry.elearn.rwth-aachen.de) durchgeschaut werden. Für ein Beispiel, wie die verschiedenen Bestandteile
eines Events auf xAPI Attribute abgebildet werden können, kann die Masterarbeit zum Datacollector konsultiert werden.
Attribute welche der Datacollector selbstständig generiert, sind die `registration`, welche eine Game Session ID wiederspiegelt,
ein `timestamp` der den Zeitpunkt einfängt, am dem das Event stattfand und die `id` des Statements selber, an welcher
jedes Statement erkannt werden kann.

Die Rückgabe der Funktion ist ein `Promise`. Was Promises sind und wie sie gehandhabt werden, kann [hier](https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Using_promises)
nachgelesen werden. Für den Fall, dass das Promise dieser Funktion aufgelöst wird, erhält man die ID des Statements, welches
an die Datenbank geschickt wurde. Im Fall eines Fehlers, erhält man den Fehler als Rückgabe. Wie die Funktion benutzt wird
kann auch in der [game.js](demo/dev/js/game.js) des Demo Spiels nachgeschaut werden.

### Statements Erhalten

Wenn ein spezielles Statement erfragt werden soll und die ID des Statements bekannt ist, erhält man dieses mit der `getStatementById(id)`
Funktion. Die Rückgabe ist hierbei auch wieder ein `Promise`. Wenn nach Statements mit bestimmten Attributen gesucht wird,
z.B. allen Statements einer Game Session, benutzt man die `getStatements(searchParams)` Funktion. Welche Suchkriterien
zulässig sind und wie diese zu bezeichnen sind, ist im [dritten Teil](https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#partthree)
der xAPI Spezifikation erklärt. Das `Promise` welches zurückgegeben wird löst sich zu einem Array mit xAPI Statements auf.
Beispiele für das Erhalten von Statements sind auch in der [game.js](demo/dev/js/game.js) des Demo Spiels gegeben.

## Offline Nutzung

Durch die Verwendung eines [Service Workers](https://developer.mozilla.org/de/docs/Web/API/Service_Worker_API/Using_Service_Workers)
kann ein Spiel trotz nicht vorhandener Verbindung zu der Datenbank gespielt werden. Alle Events, die während des Spiels
auftreten, werden gecachet und gesendet sobald eine Serververbindung wiederhergestellt wurde. Dabei können ganze Game Sessions
offline gespielt werden und der Cache bleibt sogar über Browsersitzungen hinaus persistent. **Vorsicht:** Durch Browsereinstellungen,
welche nach dem Schließen des Browsers Cookies löschen und Caches leeren, können, im Falle des Schließen des Browsers bevor
die Verbindung wiederhergestellt wurde, Events verloren gehen.
